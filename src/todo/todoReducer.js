const InitialState = { description: '', list: [] }

const todoReducer = (state = InitialState, action) => {
    switch(action.type) {
        case 'DESCRIPTION_CHANGED':
            return { ...state, description: action.payload }
        case 'TODO_SEARCH':
            return { ...state, list: action.payload }
        case 'TODO_ADD':
        case 'TODO_CLEAR':
            return { ...state, description: '' }
        default:
            return state
    }
}

export default todoReducer