import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Grid from "../template/grid";
import IconButton from "../template/iconButton";
import { add, changeDescription, clear, todoSearch } from "./todoActions";

class Form extends Component {
    constructor(props) {
        super(props);
        this.keyHandler = this.keyHandler.bind(this)
    }

    componentWillMount() {
        this.props.todoSearch()
    }

    keyHandler(e) {
        const {add, todoSearch, description, clear } = this.props;

        if(e.key === 'Enter') {
            e.shiftKey ? todoSearch() : add(description)
        } else if(e.key === 'Escape') {
            clear()
        }
    }

    render() {
        const {add, todoSearch, description, clear } = this.props;

        return (
            <div role="form" className="todoForm">
            <Grid cols="12 9 10">
                <input 
                type="text" 
                id="description" 
                className="form-control" 
                placeholder="Add new task" 
                value={description} 
                onChange={this.props.changeDescription} 
                onKeyUp={this.keyHandler}
                />
            </Grid>
            <Grid cols="12 3 2">
                <IconButton 
                    style="primary" 
                    icon="plus" 
                    onClick={() => add(description)} />
                <IconButton 
                    style="info" 
                    icon="search" 
                    onClick={() => todoSearch()} />
                <IconButton 
                    style="default" 
                    icon="close" 
                    onClick={() => clear()} />
            </Grid>
        </div>
        )
    }
}

const mapStateToProps = state => ({description: state.todo.description})
const mapDispatchToProps = dispatch => 
    bindActionCreators({ changeDescription, todoSearch, add, clear }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Form)