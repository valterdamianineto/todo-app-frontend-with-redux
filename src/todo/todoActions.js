import Axios from "axios"

const url = `http://localhost:3003/api/todos`

export const changeDescription = event => ({
    type: 'DESCRIPTION_CHANGED',
    payload: event.target.value
})

export const todoSearch = () => {
    return (dispatch, getState) => {
        const description = getState().todo.description
        const search = description ? `&description__regex=/${description}/` : ''
        const request = Axios.get(`${url}?sort=-createdAt${search}`)
        .then(resp => dispatch({ type: 'TODO_SEARCH', payload: resp.data }))
    }
}

export const add = description => {
    return dispatch => {
        Axios.post(url, { description })
            .then(resp => dispatch(clear()))
            .then(resp => dispatch(todoSearch()))
    }
}

export const markAsDone = todo => {
    return dispatch => {
        Axios.put(`${url}/${todo._id}`, {...todo, done: true})
        .then(resp => dispatch(todoSearch()))
    }
}

export const markAsPending = todo => {
    return dispatch => {
        Axios.put(`${url}/${todo._id}`, {...todo, done: false})
        .then(resp => dispatch(todoSearch()))
    }
}

export const todoRemove = todo => {
    return dispatch => {
        Axios.delete(`${url}/${todo._id}`)
        .then(resp => dispatch(todoSearch()))
    }
}

export const clear = () => {
    return [{type: 'TODO_CLEAR'}, todoSearch()]
}