import React from "react";
import PageHeader from '../template/pageHeader';

export default props => (
    <div>
        <PageHeader name='About' />
        <h2>About Us</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores officia blanditiis labore vero cumque. Odio libero exercitationem nobis doloremque suscipit. Recusandae, sit minus quaerat voluptates iure asperiores ipsa nihil cumque.</p>
        <h2>Our History</h2>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptates vel voluptatem error, nobis saepe quis quos quasi illo at aliquid voluptatum accusamus dolorum inventore nesciunt placeat temporibus iure odio sit.</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sequi ratione nisi commodi dolores, excepturi placeat culpa est iusto officia. Perferendis odio laboriosam aliquid consequuntur id non temporibus rem tenetur doloremque.</p>
    </div>
)